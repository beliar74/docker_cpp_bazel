FROM ubuntu:19.04

RUN apt-get update && \
    apt-get install --no-install-recommends -y wget ca-certificates git \
    pkg-config zip g++-9 gcc-9 zlib1g-dev unzip python3 && \
    wget https://github.com/bazelbuild/bazel/releases/download/0.29.1/bazel-0.29.1-linux-x86_64 \
    -O /usr/bin/bazel && \
    rm -rf /var/lib/apt/lists/* && \
    chmod +x /usr/bin/bazel && \
    update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 900 && \
    update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-9 900 && \
    bazel --version